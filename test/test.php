<?php
require_once "../vendor/autoload.php";

$obj = new \composertest\hello\HelloWorld();
echo $obj->hello();

echo "\n";
$obj = new \composertest\hello\HelloWorld('My Goddess');
echo $obj->hello();
